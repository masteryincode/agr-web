﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using FastColoredTextBoxNS;
namespace AGR_WEB
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();

            rtb.CaretColor = System.Drawing.Color.Green;
            rtb.AutoCompleteBrackets = true;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
           Close();
        }

        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Create a SaveFileDialog to request a path and file name to save to.
            SaveFileDialog saveFile1 = new SaveFileDialog {
                DefaultExt = "*.html",
                Filter = @"HTML Files|*.html"
            };

            // Initialize the SaveFileDialog to specify the RTF extension for the file.

            // Determine if the user selected a file name from the saveFileDialog.
            if (saveFile1.ShowDialog() == DialogResult.OK && saveFile1.FileName.Length > 0)
            {
                // Save the contents of the RichTextBox into the file.
                //richTextBox1.SaveFile(saveFile1.FileName, RichTextBoxStreamType.PlainText);
                rtb.SaveToFile(saveFile1.FileName, Encoding.UTF8);
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void rtb_TextChanged(object sender, TextChangedEventArgs e)
        {
            wbWindow.DocumentText = rtb.Text;
            var tb = (FastColoredTextBox)sender;

            //highlight html
            tb.SyntaxHighlighter.InitStyleSchema(Language.HTML);
            tb.SyntaxHighlighter.HTMLSyntaxHighlight(tb.Range);
            tb.Range.ClearFoldingMarkers();
            //find PHP fragments
            foreach (var r in tb.GetRanges(@"<\?php.*?\?>", RegexOptions.Singleline))
            {
                //remove HTML highlighting from this fragment
                r.ClearStyle(StyleIndex.All);
                //do PHP highlighting
                tb.SyntaxHighlighter.InitStyleSchema(Language.PHP);
                tb.SyntaxHighlighter.PHPSyntaxHighlight(r);
            }
        }
    }
}
